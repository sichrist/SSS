from TekTDS2000 import *
import numpy as np

FREQ = [100,
        200,
        300,
        400,
        500,
        700,
        850,
        1000,
        1200,
        1500,
        1700,
        2000,
        3000,
        4000,
        5000,
        6000,
        10000]

def readAudio():
    scope = TekTDS2000()
    x_mic,y_mic = scope.getData(1)
    x_src, y_src = scope.getData(1)
    del scope
    return {"mic" : [x_mic,y_mic], "src" : [x_src,y_src]}

def save(data,prefix):
    for item in data:
        np.savetxt(prefix+"_"+item+".csv",data[item],delimiter=',')
        print("saved: "+prefix+"_"+item+".csv")
def read(file):
    data = np.genfromtxt("100Hz_BIG_src.csv",delimiter=',')
    print(data)

def V2():
    input_key = ""
    i = 0
    type_freq = ""
    file_name = ""
    type_freq = input("Type of Freq: ")
    type_Box = input("Box : ")
    file_name = "Hz_" + type_Box + "_" + type_freq + "_v2"
    while i < len(FREQ):
        if file_name == "":
            print("Enter A filename first...")
            break

        input_key = input("Press Enter: \n")
        if input_key == "q":
            break
        elif input_key == "":
            data = readAudio()
            save(data, str(FREQ[i]) + file_name)
            i += 1

    print("Finished")

def V1():
    i = 0
    filename = "Mundharmonika"
    while True:
        input_key = input("Press Enter: \n")
        if input_key == "q":
            break
        elif input_key == "":
            data = readAudio()
            file_name = filename+"_"+str(i)
            save(data, file_name)
            i += 1
    print("Finished")

def main():
    V1()


if __name__ == '__main__':
    main()