#!/bin/python
import numpy as np
from TekTDS2000 import *
import matplotlib.pyplot as plt

FREQ = [100,
        200,
        300,
        400,
        500,
        700,
        850,
        1000,
        1200,
        1500,
        1700,
        2000,
        3000,
        4000,
        5000,
        6000,
        10000]


def readCSV(filename):
	
	try:
		inp_mic = np.genfromtxt("csv/"+filename+"_mic.csv",delimiter=',')
		inp_src = np.genfromtxt("csv/"+filename+"_src.csv",delimiter=',')
		
	except:
		return None

	return {"src" : [inp_src[0],inp_src[1]], "mic" : [inp_mic[0],inp_mic[1]]}

def plot(x,y):
	plt.plot(x,y)
	plt.show()

def periode(data,radius=1):
	x = data[0]
	y = data[1]
	maxima = []
	minima = []
	hold = (0,0)	

	# steigung berechnen
	for i in range(radius,len(y)):
		if radius + i > len(y)-1:
			break
		if i - radius < 0:
			continue
		if y[i-radius] == 0 or y[i+radius]==0:
			continue
		
		a_1 =(y[i-1] - y[i])/radius
		a_2 =(y[i] - y[i+1])/radius
		
		if a_1 < 0 and a_2 > 0:
			maxima.append(i)
		if a_1 > 0 and a_2 < 0:
			minima.append(i)

	return maxima,minima

def erode(maxima,minima,x,y,percent=0):
		
	max_mean = np.median(y[maxima])
	min_mean = np.median(y[minima])

	index_max = np.where(y > (max_mean + (max_mean * percent)))
	index_min = np.where(y < (min_mean + (min_mean * percent)))
	return index_max,index_min

def groupFreq(x,y,maxima,minima):
	dist_maxima = []
	dist_minima = []

	for i in range(1,len(minima[0])):
		dist_minima.append(minima[0][i]-minima[0][i-1])
	
	for i in range(1,len(maxima[0])):
		dist_maxima.append(maxima[0][i]-maxima[0][i-1])
	
	#dist_maxima = np.array(sorted(dist_maxima, reverse=True))
	#dist_minima = np.array(sorted(dist_minima, reverse=True))

	dist_minima = np.array(dist_minima)
	dist_maxima = np.array(dist_maxima)

	max_max = np.max(dist_maxima) / 3
	max_min = np.max(dist_minima) / 3
	

	group_min = dist_minima[np.where(dist_minima > max_min)]
	group_max = dist_maxima[np.where(dist_maxima > max_max)]

	tmp_a = np.where(np.isin(dist_minima , group_min))
	tmp_b = np.where(np.isin(dist_maxima , group_max))

	indices_min = []
	indices_max = []
	for i in tmp_a:
		indices_min.append(i-1)
		indices_min.append(i)
	for i in tmp_b:
		indices_max.append(i-1)
		indices_max.append(i)

	
	print(indices_min)
	return np.array(indices_max),np.array(indices_min)


def MaxMin(x,y):
	y = y - np.mean(y)
	input_key = ""
	maxima,minima = periode([x,y],radius=1)
	while input_key != "q":
		maxima,minima = erode(maxima,minima,x,y)
		x_max = x[maxima]
		y_max = y[maxima]
		x_min = x[minima]
		y_min = y[minima]

		if len(maxima[0]) == 0 or len(minima[0]) == 0:
			print("Min or max = 0")
			break

		m,n = groupFreq(x,y,maxima,minima)
		x_min = x[m]
		y_min = y[m]
		plt.plot(x,y,"",x_min,y_min,"or",x_max,y_max,"ob")
		plt.show()
		input_key = input("Enter >>")
		if input_key == "e":
			exit(0)

def Mundharmonika():
	i = 0
	filename = "Mundharmonika"
	files = {}
	while True:
		inp = readCSV(filename+"_"+str(i))
		if not inp:
			print("read None")
			print("Mundharmonika finished")
			break
		files[filename+"_"+str(i)] = inp
		i+=1

	for elem in files:
		
		x=files[elem]["mic"][0]
		y=files[elem]["mic"][1]
		MaxMin(x,y)

def readMic(tp):
	lab = ["Big","Small"]
	versuch = ["daten1","daten2"]
	#filename = "Hz_" + type_Box + "_" + type_freq + "_v2"
	data = {}
	d = {}
	for elem in lab:
		template = "Hz_"+elem+"_Sin"+tp
		stack = {}
		for freq in FREQ:
			filename = str(freq)+template
			stack[filename] = readCSV(filename)
		d[elem] = stack
	return d
	
def Mic():

	data = readMic("")

	for elem in data:
		for freq in data[elem]:
			print(freq)




def main():
	Mundharmonika()
	#Mic()

if __name__ == '__main__':
    main()

